var game = new Phaser.Game("100%", "100%", Phaser.AUTO, 'game_div');
var first_time = true;
var current_position = 0;
var total_animal = 2;
// Creates a new 'main' state that will contain the game
var main_state = {
    preload: function () {
        this.load.image( 'nutTrai' , 'img/left-arrow.png', 50, 50 );
        this.load.image( 'nutPhai' , 'img/right-arrow.png', 50, 50  );

        // Hình động vật
        this.load.image( 'img_0' , 'assets/images/chim-se.jpg' );
        this.load.image( 'img_1' , 'assets/images/meo.jpg' );

        this.load.audio( 'audio_0', 'assets/audios/bird.mp3');
        this.load.audio( 'audio_1', 'assets/audios/cat.mp3');
    },

    create: function() {
        // Backbround & helptext
        this.setupBackground();
        this.text1 = this.add.text( this.game.width / 2, this.game.height / 2 - 60, this.game.width + '-' + this.game.height, { font: '72px serif' , fill : '#fff' } );
    },

    update: function () {
    },

    render: function() {
    },

    setupBackground: function () {

        this.animal = this.add.tileSprite( 0, 0, this.game.width, this.game.height, 'img_0' );
        first_time = first_time = false;

        this.sound.volume = 1;

        this.leftArrow = this.game.add.button( 20, this.game.height - 80, 'nutTrai', this.moveLeft, this);
        this.rightArrow = this.game.add.button( this.game.width - 70, this.game.height - 80, 'nutPhai', this.moveRight, this);
    },

    moveLeft: function(){
        if(!first_time){
            if(current_position > 0)
            {
                current_position --;
            }
            this.animal.loadTexture('img_' + current_position, 0);
            this.add.audio( 'audio_' + current_position ).play();
        } 
    },

    moveRight: function(){
        if(!first_time){
            if(current_position < total_animal - 1)
            {
                current_position ++;
            }
            this.animal.loadTexture('img_' + current_position, 0);
            this.add.audio( 'audio_' + current_position ).play();

        } 
    }
};

// Add and start the 'main' state to start the game
game.state.add('main', main_state); 
game.state.start('main');